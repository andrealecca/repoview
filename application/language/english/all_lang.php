<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$dir = APPPATH.'language/english/';
if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false ) {
            if($file != '.' && $file != '..' &&  $file != 'all_lang.php' && $file != 'index.html')
            {
                
                require $file;
            }
            
        }
        closedir($dh);
    }
}

?>
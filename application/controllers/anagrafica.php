<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anagrafica extends CI_Controller {


	public function index($email)
	{
               $this->load->model('anagrafica_model','anagrafica');
               
               $dati['dativari'] = $this->anagrafica->get_dati($email);
               
              
            
	       $this->load->view('anagrafica',$dati);
	}
        
        

}

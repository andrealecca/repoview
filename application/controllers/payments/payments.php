<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payments extends CI_Controller {
	
	
	
	
	public function gotopay(){
		
		// parametri da file di configurazione
		$this->load->config('payments');	
	    $data['url_paypal'] = $this->config->item('url_paypal');
        $data['business_paypal'] = $this->config->item('business_paypal');
        $data['site_url'] = BASE_URL;
		
	
		
		
		$this->load->model('payments/payments_model','payments');
		
		$this->payments->ArrayComponents['type'] = 'Distribution Payments';
		$this->payments->ArrayComponents['user_id'] = '1';
		$this->payments->ArrayComponents['refs_id'] = '1';
		$this->payments->ArrayComponents['name'] = 'Andrea';
		$this->payments->ArrayComponents['surname'] = 'Lecca';
		$this->payments->ArrayComponents['fiscalcode'] = 'LCCNDR80M31B354G';
		$this->payments->ArrayComponents['address'] = 'VIA CAGLIARI 114 DECIMOMANNU';
		$this->payments->ArrayComponents['total_amount'] = '10.00';
		$this->payments->ArrayComponents['currency'] = 'EUR';
		
		// TODO: metti un foreach che scorre l'array e popola i valori con il post
		
		$this->payments->ArrayComponents['result_payment'] = 'N'; // in questa fase sempre a N
		
		// Questo è il paramentro che ci viene restituito dall'inserimento
		$item_name = $this->payments->setParams();
		
		
		

			$Link=$data['url_paypal'];
						 $link.=http_build_query(
							 $data = array(
								 "cmd" => "_xclick",
								 'business'=>$data['business_paypal'],
								 "lc" => "IT",
								 "item_name" => $item_name,
								 "amount" => $this->payments->ArrayComponents['total_amount'],   // a variable acquired value
								 "currency_code" => $this->payments->ArrayComponents['currency'],
								 "button_subtype" => "services",
								 "cn" => "Add special instructions to the seller",
								 "return" => $data['site_url'].'/payments/paypal/return_page_paypal/'.$item_name,
								 "notify_url"=> 'http://www.andrealecca.it/andrea.php',  // $data['site_url'].'/payments/paypal/notify_paypal',
								 "cancel_return"=>$data['site_url'].'/payments/paypal/cancel_payment_paypal/'.$item_name,
							 )
						 );
						 header( 'Location: '.$Link.'?'.$link );
						 die();
				
	}			
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paypal extends CI_Controller {

	
	
	// Questa è la pagina che viene caricata quando un utente ritorna da paypal
	// non deve contenere logica perchè è solo di avviso
	public function return_page_paypal($id_payment){   
         
		var_dump("Pagina di ritorno dal pagamento !!!");
		$this->load->model('payments/payments_model','payments');
		
		$this->payments->ArrayComponents['id'] = $id_payment;
		$this->payments->getParams();
    }
    
	// Questa è la pagina che viene richiamata quando un utente cancella
	// il pagamento, magari possiamo invitarlo a ritentare
    public function cancel_payment_paypal($id_payment){
		var_dump("Pagina di cancellazione del pagamento");
		
		var_dump($_POST);
		
		$this->load->model('payments/payments_model','payments');
		
		$this->payments->ArrayComponents['id'] = $id_payment;
		$this->payments->getParams();
		
		// da settare
		$user_id = 1;
		// TODO: cambia con il reale userid dell'utente
		
		
		var_dump("Ricordati di settare la user_id");
		
		if($this->payments->ArrayComponents['user_id'] == $user_id && 
				$this->payments->ArrayComponents['result_payment'] == 'N'){
			
			$this->payments->ArrayComponents['note'] = 'Pagamento annullato dall\'utente';
			$this->payments->saveData();
		}
		
		
		var_dump($this->payments->ArrayComponents);
		//$this->payments->ArrayComponents['type'] = 'Distribution Payments';
		
		//$item_name = $this->payments->getParams();
		
		
		
		
		
          
    }
                


         
    public function notify_paypal() {    
            
        // richiamo i paramentri dal file di configurazione    
		$this->load->config('payments');	
	    $data['url_paypal'] = $this->config->item('url_paypal');
        $data['business_paypal'] = $this->config->item('business_paypal');
        $data['site_url'] = BASE_URL;
            
                // STEP 1: Read POST data

                // reading posted data from directly from $_POST causes serialization 
                // issues with array data in POST
                // reading raw POST data from input stream instead. 
                $raw_post_data = file_get_contents('php://input');
                
               
                
                
                $raw_post_array = explode('&', $raw_post_data);
                $myPost = array();
                foreach ($raw_post_array as $keyval) {
                  $keyval = explode ('=', $keyval);
                  if (count($keyval) == 2)
                     $myPost[$keyval[0]] = urldecode($keyval[1]);
                }
                
                
               
                
                // read the post from PayPal system and add 'cmd'
                $req = 'cmd=_notify-validate';
                if(function_exists('get_magic_quotes_gpc')) {
                   $get_magic_quotes_exists = true;
                } 
                foreach ($myPost as $key => $value) {        
                   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
                        $value = urlencode(stripslashes($value)); 
                   } else {
                        $value = urlencode($value);
                   }
                   $req .= "&$key=$value";
                }


                // STEP 2: Post IPN data back to paypal to validate

                $ch = curl_init($data['url_paypal']);
                    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                    curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

                // In wamp like environments that do not come bundled with root authority certificates,
                // please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
                // of the certificate as shown below.
                // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
                    
                    
              
                   
                    
                if( !($res = curl_exec($ch)) ) {
                    // error_log("Got " . curl_error($ch) . " when processing IPN data");
                    curl_close($ch);
                    exit;
                }
                curl_close($ch);
                
                 
             

                // STEP 3: Inspect IPN validation result and act accordingly
                        
                //   if (strcmp ($res, "VERIFIED") == 0) {
                
                if (strcmp ($res, "VERIFIED") == 0) {
                    // check whether the payment_status is Completed
                    // check that txn_id has not been previously processed
                    // check that receiver_email is your Primary PayPal email
                    // check that payment_amount/payment_currency are correct
                    // process payment

                    // assign posted variables to local variables
                    
                    $item_name = $_POST['item_name'];
                    $item_number = $_POST['item_number'];
                    $payment_status = $_POST['payment_status'];
                    $payment_amount = $_POST['mc_gross'];
                    $payment_currency = $_POST['mc_currency'];
                    $txn_id = $_POST['txn_id'];
                    $receiver_email = $_POST['receiver_email'];
                    $payer_email = $_POST['payer_email'];
                    
                    $this->load->model('payments/payments_model','payments');
		
					$this->payments->ArrayComponents['id'] = $id_payment;
					$this->payments->getParams();
					
					
					if($this->payments->ArrayComponents['total_amount'] <= $payment_amount &&
							$this->payments->ArrayComponents['total_amount'] == $payment_currency
							){

						$this->payments->ArrayComponents['result_payment'] = 'Y';
						$this->payments->ArrayComponents['note'] = 'Paypal pagamento avvenuto con successo';
						$this->payments->saveData();
						
						// TODO: metti un invio di mail
						
					}else{
						
			
						$this->payments->ArrayComponents['note'] = 'Paypal totale pagamento o currency non corrispondono';
						$this->payments->saveData();
						
						// TODO: metti un invio di mail
						
					}
					
				
                    
                } else if (strcmp ($res, "INVALID") == 0) {
                    // log for manual investigation
					
					
					
					if(isset($_POST['item_name'])){
						
						$this->load->model('payments/payments_model','payments');
		
						$this->payments->ArrayComponents['id'] = $_POST['item_name'];
						$this->payments->getParams();
			
						$this->payments->ArrayComponents['note'] = 'Paypal ha restituito INVALID';
						$this->payments->saveData();
					}
					
					// TODO: metti un invio di mail
					
					
                }




        } // end method return_paypal
	
	
	
	
	
	
  
}

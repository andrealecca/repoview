<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * Andrea Lecca
 * andrea.lecca@tiscali.it
 * 
 * 
 */


function directory_admin($array_session) {	
	
	if($array_session['admin']){
		$directory = 'auth_admin';
	}elseif($array_session['super_admin']){
		$directory = 'auth_super_admin';
	}else{
		$directory = 'auth_users';
	}
		
    return $directory;
}
?>  
<?php

/*
 * 
 * Andrea Lecca
 * 
 * Questo file è di test per i form
 * 
 */


// codice è codice

// $config["NOME NEL DATABASE"] = "ETICHETTA DEL CAMPO";


$config["test"] = array(
			
			'form_fields'=> array(

				'1'=> array(
					'name'=>'anagrafica[nome]',
					'value'=>'name',
					'type'=> 'text',
					'rules'=>'required',
					'label'=> 'label nome'
				),
				'2'=>array(
					'name'=>'anagrafica[cognome]',
					'value'=>'cognome',
					'type'=> 'text',
					'rules'=>'required',
					'label'=> 'cognome'
				),
				'3'=>array(
					'name'=>'cont_anagrafica[citta]',
					'value'=>'citta',
					'type'=> 'text',
					'rules'=>'required',
					'label'=> 'città',
                                        
				),		
				'4'=>array(
					'name'=>'cont_anagrafica[eta]',
					'value'=>'eta',
					'type'=> 'text',
					'rules'=>'required|is_numeric',
					'label'=> 'eta'
				)
				
				
				
				
				
			),
			
			'others_existents_fields' => array(
				
				
			),
			
			'tables_update_orders'=> array(
				
				
				'anagrafica',
				'cont_anagrafica'
				
			),
			
			'relations'=>array(
				
			)
            
			
		);








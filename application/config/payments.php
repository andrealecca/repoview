<?php
/*
 * 
 * Author: Andrea Lecca
 * Parametri di configurazione del pagamento
 * 
 * Per i test:
 * 

 * 
 */

//### PAYPAL, sandbox di test

if($_SERVER['SERVER_NAME'] == 'www.andrealecca.it'){
	// diretto su paypal
	$config['url_paypal'] = 'https://www.paypal.com/cgi-bin/webscr';
	$config['business_paypal'] = '';
}else{
	// per test su sandbox
	$config['url_paypal'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
	$config['business_paypal'] = 'alecca_1295864436_biz@arcaservices.it';
}





?>

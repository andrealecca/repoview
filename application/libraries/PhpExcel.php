<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 *  ======================================= 
 *  Author     : Muhammad Surya Ikhsanudin 
 *  License    : Protected 
 *  Email      : mutofiyah@gmail.com 
 *   
 *  Dilarang merubah, mengganti dan mendistribusikan 
 *  ulang tanpa sepengetahuan Author 
 *  ======================================= 
 */  
require_once $_SERVER['DOCUMENT_ROOT'].'/'.APPPATH."third_party/PHPExcel2.php"; 

class PhpExcel extends PHPExcel2 { 
    public function __construct() { 
        parent::__construct(); 
    } 
}
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Form_save_utils 
{
    
    
    
    	var $array_to_save = array();
	var $array_clean;
    var $others_existents_fields;
    
    
    
    
    
	public function __construct()
	{
		$this->CI =& get_instance();
	}
	
        
        
        
        
	
	public function create_form_object($form_name,$array_query,$passed_post_values){
		
		
		
        
		$this->CI->config->load('form/'.$form_name);
		$form_conf = $this->CI->config->item('test');	
			
		$jsonObj_ = json_encode($form_conf['form_fields']);
        $jsonObj = json_decode($jsonObj_);
		
        if($array_query){ //verifico che non sia vuoto -- è risultato di una query...
         // popolo con i valori del database
		foreach ($array_query[0] as $key => $value) {
			
               
				
                // mette i valori nell'array
                
                    $field_to_entry = $this->searchForId($key, $jsonObj);

                    if (isset($field_to_entry))
                        $jsonObj->$field_to_entry->value = $value;
                
            }   
        }
		// popolo nel caso esista con i valori passari con il submit
		// se invece ho fatto il submit, devo popolare con questi valori
		
		
		// questo perchè alcune chiavi hanno piu valori...perchè per ogni tabella posso avere tanti valori
		$passed_post_valuesb=array();
		$i=0;
		
		foreach($passed_post_values as $keysa=>$valuesa){
			
			if(is_array($valuesa)){
				foreach($valuesa as $aa=>$bb){
				
					$passed_post_valuesb[$i++][$keysa] = array($aa =>$bb);
					
				}
			}
		}
		
		
		foreach($passed_post_valuesb as $keys=>$values){
			
			
			
			if(is_array($values)){
				// mi recupero parte del post
				$newKey = array_keys($values);
				$newKey = $newKey[0];
				
				$part_of_value = $values[$newKey];
				
				$newKey2 = array_keys($values[$newKey]);
				$newKey2  = $newKey2[0];
				
				$final_value = $part_of_value[$newKey2];
				
				
				
				$newValue = $values[$newKey];

				// mi recupero come entrare nel form
				$field_to_entry = $this->searchForId($newKey.'['.$newKey2.']', $jsonObj);

						// risetto il valore !!!
						if(isset($field_to_entry))
							$jsonObj->$field_to_entry->value = $final_value;
			}		
			
		}		
		
		
		
		
		
		
		
		return $jsonObj;
		
	}
	
    
    
    public function to_save_form_object($form_name,$formObject){
		
		$this->CI->config->load('form/'.$form_name);
		$form_conf = $this->CI->config->item($form_name);
        
        $form_conf['form_fields'] = $formObject;
        
        return $form_conf;
    
    }
	
	public function sort_table_to_save($form_data_save){
				
		foreach ($form_data_save as $key => $value) {

				$old_key = key($value);
				$final_value = $value[$old_key];


				// devo differenziare il caso in cui salvo in una sola tabella o in piu tabelle
				// basta controllare se ho una [
				$trovato = strpos($old_key, '[');

				if ($trovato != false) {

					// mi ottengo un qualcosa del tipo nometabella[nomecolonna]
					// elimino la parentesi finale
					$string = str_replace(']', '', $old_key);
					// con un explode mi ottengo da una parte il nome della tabella e il nome della colonna
					list($table, $column) = explode('[', $string);

					// mi metto in un array tutto ordinato per il salvataggio
					$array_to_save[$table][$column] = $final_value;
				} else {

					$array_to_save['table'][$old_key] = $final_value;
				}
			}
			
			
			return $array_to_save;
			
		}
	
		
		
		
		
		
		// other utils functionality
		
		
		
		function searchForId($valore, $array) {	
			
			foreach ($array as $key => $value) {			
				if(array_search($valore,(Array)$value)){
					return $key;
				}
			}		
		 }
		
	function mysql_rows_with_columns($query) {
		$result = mysql_query($query);
		if (!$result) return false; // mysql_error() could be used outside
			$fields = mysql_num_fields($result);
			$rows = array();
		while ($row = mysql_fetch_row($result)) { 
			$newRow = array();
			for ($i=0; $i<$fields; $i++) {
				$table = mysql_field_table($result, $i);
				$name = mysql_field_name($result, $i);
				$newRow[$table . "[$name]"] = $row[$i];
			}
			$rows[] = $newRow;
		}
		mysql_free_result($result);
		return $rows;
	}
		
		
        
        
          /**
     * Metodo generale per il salvataggio delle informazioni
     * 
     * @return boolean
     * 
     */
    
    public function save_data(){
		
		
		
		// inserisco i valori dei campi che devo solo editare e non inserire exnovo
		
		if(isset($this->others_existents_fields)){
			$this->array_to_save['others_existents_fields'] = $this->others_existents_fields;
		}
		
		
		$this->clean_data_insert();
		
		
        // faccio il foreach delle tabelle da salvare
        foreach ($this->array_to_save['tables_update_orders'] as $table) {
            
			// controllo se devo fare un update or un insert
			$where_fields_setted = $this->controll_action($table);
			
			if($where_fields_setted != false){
				
				$this->update_table($table,$where_fields_setted);
	
			}else{
				
				$datas = $this->insert_table($table);
				$where_fields_setted['id'] = $datas;
			}

			// controlliamo se ci sono dei campi che devono essere inseriti da altre parti
            //con questa funzione lascio un riferimento se necessario alle tabelle successive.
			$this->control_successive_insert($table,$where_fields_setted);
        }
		
		
		return true;
		
    }
   
	/**
     * Metodo che preso in input la tabella verifica la presenza di un riferimento 
     * nella variabile array_to_save['others_existents_fields'].
     * 
     * @param type $table
     * @return boolean
     * 
     */
	public function controll_action($table){
		
		
		// quindi per questa tabella controllo le azioni da fare
		foreach($this->array_to_save['others_existents_fields'] as $fieldskey => $fieldsvalue){
			
			$trovato = strpos($fieldskey, $table.'[');
			
			if ($trovato !== false) {
				$string = str_replace(']', '', $fieldskey);
				list($table, $value_field) = explode('[', $string);
				return array($value_field => $fieldsvalue);
			}
		}

		return false;
	}

	/**
     * Metodo che gestisce ed estrae dalla variabile $array_to_save
     * solo il nome ed il valore di campi
     * 
     */
	public function clean_data_insert(){
		
		
		$array_clean = array();
		
		foreach($this->array_to_save['form_fields'] as $key=>$value){	
			$string = str_replace(']', '', $value->name);
			list($table, $value_field) = explode('[', $string);
			$array_clean[$table][$value_field] = $value->value;
		}
		$this->array_clean = (Object)$array_clean;
		
	}
	
	/**
     * 
     * Metodo per l'update della tabella. E'chiamato quando è popolata
     * la variabile $where_fields_setted 
     * 
     * @param type $table
     * @param type $where_fields_setted
     */
	
	
	public function update_table($table, $where_fields_setted) {
        $newKey = array_keys($where_fields_setted);
        $newKey = $newKey[0];
        $newValue = $where_fields_setted[$newKey];
        $array_clean = (Array) $this->array_clean;
        $this->CI->db->where($newKey, $newValue);
		
        $this->CI->db->update($table, $array_clean[$table]);
    }

    /**
     * Metodo per l'insermento e creazione nuovo record nella tabella
     * restituisce l'ultimo id inserito.
     * 
     * @param type $table
     * @return type
     * 
     */
    public function insert_table($table) {
        $array_clean = (Array) $this->array_clean;
		
        $this->CI->db->insert($table, $array_clean[$table]);
        return $this->CI->db->insert_id();
    }
	
	/**
     * 
     * Metodo che inserisci nei dati dal salvare per la tabella in $table
     * i valori presenti nella variabile $where_fields_setted
     * 
     * @param type $table
     * @param type $where_fields_setted
     */
	public function control_successive_insert($table,$where_fields_setted){
		
		foreach($this->array_to_save['relations'] as $key=>$value){
			
			$trovato = strpos($key, $table.'[');
			
			if ($trovato !== false) {
			
				$before_table = $key;
				$after_table = $value;

				$stringB = str_replace(']', '', $before_table);
				list($tableB, $value_fieldB) = explode('[', $stringB);

				$stringA = str_replace(']', '', $after_table);
				list($tableA, $value_fieldA) = explode('[', $stringA);
					
			
			}
		}
		
			
			if(isset($tableA)){
				$arr = $this->array_clean->$tableA;
				$oth_values = array($value_fieldA => $where_fields_setted[$value_fieldB]);
	
				$this->array_clean->$tableA = array_merge($this->array_clean->$tableA,$oth_values);
			}
	}
	


	
}

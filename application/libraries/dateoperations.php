<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
*
* Class dateOperations
* 
* By Andrea Lecca 
* 

* Usage:
* echo '<br>'.$this->dateoperations->sum('2010-11-04','day',1,FALSE); // Prints: 2010-11-05
* echo '<br>'.$this->dateoperations->sum('2010-11-04 00:00:00','minute',15); // Prints: 2010-11-04 00:15:00
* echo '<br>'.$this->dateoperations->subtract('2010-11-04 00:00:00','second',35); // Prints: 2010-11-03 23:59:25
* echo '<br>'.$this->dateoperations->subtract('2010-11-04 00:00:00','year',1); // Prints: 2009-11-04 00:00:00
* echo '<br>'.$this->dateoperations->subtract('2010-11-04 00:00:00','day',15, FALSE); // Prints: 2010-10-20 
*/

class dateOperations {
    
    // date,(day,hours,month,years),TRUE,MYSQL
    public function sum ($date,$what,$value,$full=TRUE,$return_format='mysql') {
        
        $return = $this->calculate($date,$what,$value,'sum',$full,$return_format);
        return $return;
        
    }
    
    public function subtract ($date,$what,$value,$full=TRUE,$return_format='mysql') {
        
        $return = $this->calculate($date,$what,$value,'subtract',$full,$return_format);
        return $return;
        
    }   
    
    private function calculate($date,$what,$value,$operation,$full,$return_format) {
        
        // checking args
        if($operation!='sum' && $operation!='subtract') return FALSE;
        if ($what!='day' && $what!='month' && $what!='year' && $what!='hour' && $what!='minute' && $what!='second') return FALSE;

        // validating date or datetime
        if (!preg_match('/\\A(?:^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\\s(((0?[0-9])|(1[0-9])|(2[0-3]))\\:([0-5][0-9])((\\s)|(\\:([0-5][0-9])))?))?$)\\z/', $date)) {
            return FALSE;
        }
        
        /* From this point I'm sure its a valid date or datetime no matter what */
        
        $only_date = substr($date, 0, 10);
        $only_time = substr($date, 11, 8);
        
        list($year, $month, $day) = explode("-", $only_date);
        
        if ($what=='day')       $day    = $operation=='sum' ? intval($day) + intval($value) : intval($day) - intval($value) ;
        if ($what=='month')     $month  = $operation=='sum' ? intval($month) + intval($value) : intval($month) - intval($value);
        if ($what=='year')      $year   = $operation=='sum' ? intval($year) + intval($value) : intval($year) - intval($value);
    
        $hour = $minute = $second = 0;
    
        if($only_time!='') { // we have time too!
            
            list($hour, $minute, $second)   = explode(":", $only_time);
            
            if ($what=='hour')      $hour   = $operation=='sum' ? intval($hour) + intval($value) : intval($hour) - intval($value);
            if ($what=='minute')    $minute = $operation=='sum' ? intval($minute) + intval($value) : intval($minute) - intval($value);
            if ($what=='second')    $second = $operation=='sum' ? intval($second) + intval($value) : intval($second) - intval($value);
                
        }

        $date_tmp = mktime($hour, $minute, $second, $month, $day, $year);    

        if ($return_format=='mysql') {
            if($full) $date_tmp = date('Y-m-d H:i:s', $date_tmp); // return date and time
            else $date_tmp = date('Y-m-d', $date_tmp); // return only date
        } elseif (!$return_format=='timestamp') {
            return FALSE;   
        }
                       
        return $date_tmp;   
        
    }
    
    //convert dd/mm/YYYY in YYYY-mm-dd
    public function slash_dmY_to_MYSQL_date($date) {
        
        return implode('-', array_reverse(explode('/',$date))); 
    }
    
    //convert YYYY-mm-dd in dd/mm/YYYY
    public function MYSQL_to_slash_dmY_date($date) {
        
        return implode('/', array_reverse(explode('-',$date))); 
    }
    
    //convert YYYY-mm-dd hh:mm:ss in dd/mm/YYYY
    public function MYSQL_hhmmss_to_slash_dmY_date($datetime) {
        
        $date = explode(" ", $datetime); //data in formato YYYY-mm-dd
        return $this->MYSQL_to_slash_dmY_date($date[0]); //converto in dd/mm/YYYY
    }
    
    //generate a string day and date yyyy-mm-dd
    
    public function MYSQL_stringday_($datetime){
        
        
        
         $date = explode("-", $datetime); //data in formato YYYY-mm-dd
         
         
            
         $date = mktime(0,0,0,$date[1],$date[2],$date[0]); 
         
         
         return date('l',$date);
         
        
        
        
    }
    
    // da un timestamp resituisce la stringa relativa al giorno
    
    public function MYSQL_stringday_timestamp($datetime){
                 
         
         return date('l',$datetime);
         
        
        
        
    }

    // date1 and date2 in yyyy-mm-dd hh:mm format
    public function date_compare_mag($date1,$date2){
    
        
        if(strtotime($date1) > strtotime($date2)){
            return true;
        }else{
            return false;
		}

    }
    
    // date1 and date2 in yyyy-mm-dd hh:mm format
    public function date_compare_min($date1,$date2){
        
        
        if(strtotime($date1) < strtotime($date2)){
            return true;
        }else{
            return false;
        }
                   
    }


    // date1 and date2 in yyyy-mm-dd hh:mm format
    public function date_compare_equal($date1,$date2){
        
        
        if(strtotime($date1) == strtotime($date2)){
            return true;
        }else{
            return false;
        }
                   
    }
   

	// <!-- sezione dedicata ad ottenere informazioni -->


	// restituisce la differenza tra le due date in secondi
	public function diffDate($date1,$date2){   

		$calculate = (strtotime($date1) - strtotime($date2));

			return $calculate;

	}


	
}  //end class	


// altre utilities

// data e ora di oggi date("Y-m-d H:i:s")  --> vedi http://www.allwebfree.it/php_data_e_ora.php
// strtotime -- > restituisce i secondi di una data formattata Y-m-d H:i:s
	

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Formtest_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->library('form_save_utils');
	}
	
	// --------------------------------------------------------------------

      /** 
       * function SaveForm()
       *
       * insert form data
       * @param $form_data - array
       * @return Bool - TRUE or FALSE
       */

	function SaveForm($form_data,$others_existents_fields)
	{
		
				
		
		
		$this->form_save_utils->array_to_save = $form_data;
		$this->form_save_utils->others_existents_fields = $others_existents_fields;
		return $this->form_save_utils->save_data();

	}
}
?>
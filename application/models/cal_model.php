<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cal_model extends CI_Model
{

   // data di partenza scelta dall'utente per la visualizzazione del calendario  
   var $calendar_start_date; 
   // data di fine scelta dall'utente per la visualizzazione del calendario
   var $calendar_end_date;
   
   // array creato dalla query con le occupazioni
   var $res_query = array();
   
   // array che possiede nella chiave le date, inizialmente è vuoto, ma in seguito
   // con la query viene popolato
   var $setDataArray = array();
   
   /*
    * 
    * metodo che ci crea l'array vuoto poi successivamente da popolare
    * 
    */
   
   public function setDataArray(){
     
       
        $this->load->library('dateoperations');
       
        $diff_sec = $this->dateoperations->diffDate($this->calendar_end_date,$this->calendar_start_date);
        
        $diff_days = $diff_sec/(3600*24);
        
        for($i=0;$i<=$diff_days;$i++){
            $new_date = $this->dateoperations->sum($this->calendar_start_date,'day',$i,FALSE);
            
            $array_data[$new_date] = array();
        }       
               
       
       
        $this->setDataArray =  $array_data;
   }
   
   /*
    * 
    * Query che va a pescare dal db le occupazioni
    * 
    */
    
   public function query(){
       
    
       
        $sql = "
            

               
SELECT *,
if(date_format( start_date, '%Y-%m-%d' )<'".$this->calendar_start_date."','".$this->calendar_start_date."',date_format( start_date, '%Y-%m-%d' )) as partenza ,

if(date_format( end_date, '%Y-%m-%d' ) BETWEEN '".$this->calendar_start_date."' AND '".$this->calendar_end_date."', date_format( end_date, '%Y-%m-%d' ),'".$this->calendar_end_date."') as fine, 

DATEDIFF(
    if(date_format( end_date, '%Y-%m-%d' ) BETWEEN '".$this->calendar_start_date."' AND '".$this->calendar_end_date."', date_format( end_date, '%Y-%m-%d' ),date_format('".$this->calendar_end_date."','%Y-%m-%d')),
    if(date_format( start_date, '%Y-%m-%d' )<'".$this->calendar_start_date."',date_format('".$this->calendar_start_date."','%Y-%m-%d'),date_format( start_date, '%Y-%m-%d' ))
        ) AS DiffDate
FROM `evento`
WHERE ((
date_format( start_date, '%Y-%m-%d' ) <= '".$this->calendar_end_date."'
)
AND (date_format( end_date, '%Y-%m-%d' ) >= '".$this->calendar_start_date."'
))






        ";

        
    
       
        $result = $this->db->query($sql)->result_array();
        
        $this->res_query = $result;
        
    }
    
    /*
     * 
     * Scorre l'array delle occupazioni e va a popolare l'array principale
     * delle date
     * 
     */
    
    
    public function setData(){
        
        foreach($this->res_query as $key=>$value){
            
          
            $value['DiffDate'] = $value['DiffDate']+1;
            
            for($i=0;$i<$value['DiffDate'];$i++){
                
                
                $this->load->library('dateoperations');
              
                $new_date = $this->dateoperations->sum($value['partenza'],'day',$i,FALSE);
        
                if(isset($this->setDataArray[$new_date])){
                    $this->setDataArray[$new_date][$value['id']] = $value['evento'];
                }
            }
            
            
        }
        
        
   
        
        
        
    }
    
    public function stampatutto(){
            return $this->setDataArray;
        }
          
        
}

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Struttura_model extends CI_Model
{
	
    var $nome_struttura;
    var $camere;
    var $letti;
    
    
    public function set_params(){
        
        $this->letti = 12;
        $this->camere = 6;
        
        
    }
    
    public function costo($obj){
        
        
        
        $prezzo = $obj->get_prezzo();
        
        
        return $this->letti*$prezzo;
        
        
    }
    
    
    public function book_camera($posti_prenotati){
        
        
        $this->camere = $this->camere-$posti_prenotati;
        $this->letti = $this->letti-(2*$posti_prenotati);
        
        
    }
    
    
    
    
    
    
          
        
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Payments_model extends CI_Model{
    
	
	public $ArrayComponents = array(
			'id'=>'',
			'user_id'=>'',
			'type'=>'',
			'refs_id'=>'',
			'name'=>'',
			'surname'=>'',
			'fiscalcode'=>'',
			'address'=>'',
			'total_amount'=>'',
			'currency'=>'',
			'result_payment'=>'',
			'date_payments'=>'');
	
	
	// inserisce nell oggetto e nel db i dati
	public function setParams(){
		$this->db->insert('payments', $this->ArrayComponents);
		return $this->db->insert_id();
	}
	// compone l'oggetto con tutti i parametri
	public function getParams(){
		$query = $this->db->get_where('payments', array('id' => $this->ArrayComponents['id']));
		$ArrayComponents = $query->result_array();
		$this->ArrayComponents = $ArrayComponents[0]; 
	}
	// fa l'update di un oggetto gia esistente
	public function saveData(){
		$this->db->where('id', $this->ArrayComponents['id']);
        $this->db->update('payments', $this->ArrayComponents);
	}

}
